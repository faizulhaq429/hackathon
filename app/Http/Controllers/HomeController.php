<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
	public function index()
	{
		return view('home');
	}

	public function product()
	{
		return view('menu/product');
	}

	public function brand()
	{
		return view('menu/brand');
	}

	public function contact()
	{
		return view('menu/contact');
	} 
}
